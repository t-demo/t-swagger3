package com.example.controller;

import com.example.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("hello")
    public CommonResult hello() {
        return CommonResult.success();
    }

}

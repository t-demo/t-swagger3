package com.example.controller;

import com.example.common.CommonResult;
import com.example.entity.UserRecord;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/*
Swagger的访问路径由port/swagger-ui.html改成了port/swagger-ui/index.html ，
项目演示代码在 springboot-swagger
 */
@RestController
@RequestMapping("userRecord")
public class UserRecordController {

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @ApiOperation("通过主键查询单条数据")
    @GetMapping("{id}")
    public CommonResult selectOne(@PathVariable Serializable id) {
        return CommonResult.success();
    }

    /**
     * 新增数据
     * @param userRecord 实体对象
     * @return 新增结果
     */
    @ApiOperation("新增数据")
    @PostMapping("insert")
    public CommonResult insert(@RequestBody UserRecord userRecord) {
        return CommonResult.success();
    }

    /**
     * 修改数据
     * @param userRecord 实体对象
     * @return 修改结果
     */
    @ApiOperation("修改数据")
    @PutMapping("update")
    public CommonResult update(@RequestBody UserRecord userRecord) {
        return CommonResult.success();
    }

    /**
     * 删除数据
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiOperation("删除数据")
    @DeleteMapping("delete")
    public CommonResult delete(@RequestParam("idList") List<Long> idList) {
        return CommonResult.success();
    }
}

package com.example.common;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonResult implements Serializable {

    private int code;
    private String msg;
    private Object data;

    private CommonResult(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static CommonResult success() {
        return new CommonResult(200, "success", null);
    }
}
